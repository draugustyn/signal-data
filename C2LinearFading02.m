
function C2LinearFading02(numexp)
% Linear fading 02

% model params
params.dummy = 0;
f = @(t,x) [-0.05*x(1) + 0.01*x(2);  - 0.01*x(2) - 0.001*x(1); -0.08*x(3) + 0.05*x(1)]

%experiment params
x0 = [ 3; -1; 2];
tmax = 100;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



