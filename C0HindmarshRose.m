
function C0HindmarshRose(numexp)
% Hindmarsh-Rose  model

% model params
params.a = 1;
params.b = 3;
params.c = 1;
params.d = 5;
params.r = 5 * 1e-3 % 2 * 1e-3;
params.I = 2; %3
params.s = 4;
params.xr = -8/5;


f = @(t,x) [x(2) - params.a * x(1)^3 + params.b * x(1)^2 - x(3) + params.I; 
    params.c - params.d * x(1)^2 - x(2); 
    params.r *(params.s*(x(1)- params.xr) -x(3))];

%experiment params
x0 = [-1.1;0.5;0.5];
tmax = 100;
nnsize = 1000;

% SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);

if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



