
function C0ModelRoslerA52(numexp)
% Rosler attractor 

% model params
params.a = 0.2;
params.b = 0.2;
params.c = 5.7;
f = @(t,x) [-x(2) - x(3); x(1) + params.a * x(2); params.b + x(3)*(x(1) - params.c)];

%experiment params
x0 = [-9;0;0];
tmax = 300;
nnsize = 1000;

% SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);

if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



