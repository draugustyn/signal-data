%       
% Continuous chaotic systems 
%

tic


global pPlotFig
pPlotFig = 1 ;

%3D models
C0ModelLorenzA51 ;

C0ModelRoslerA52 ;

C0ModelHalvorsenA513  ; 

%%% not recommended 
%C0ModelLabyrinthA64 ;

C0ModelRucklideA515 ;

%2D models
C0ModelUedaA45 ;


%
% Continuous non-chaotic systems 
%

% 2D models - oscilators

%C1LinearOscillator01 ;

%C1LinearOscillator02 ;


% 3D models - fading 

%C2LinearFading01 ;

%C2LinearFading02 ;


C1LinearOscFading01 ;

C1LinearOscFading02 ;

C1LinearOscBooming01 ;


XC03 ;

XC04 ;

toc
