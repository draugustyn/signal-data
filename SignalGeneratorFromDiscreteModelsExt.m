%
% Dicrete chaotic systems 
%

tic
numSamples= 1000 ; % at least 4

global pPlotFig
pPlotFig = 0 ;

%1D model

D0LogisticA11(numSamples/2);    % *
D0LogisticA11(numSamples/4,1);    % *
D0LogisticA11(numSamples/4,1);


D0CubicA15(numSamples);       % *


%2D model

D0HenonA21(numSamples);       % *

%D0HenonQadraticA32() ; % Not recommended

%
% Dicrete non-chaotic systems 
%


% 1D model - saw oscillator
%D1saw(numSamples/4); 
%D1saw(numSamples/4,1);
%D1saw(numSamples/4,1);
%D1saw(numSamples/4,1);

for rn=1:numSamples
    D1saw(1,1);
end

% 1D model - pulse oscillator
%D1pulse(numSamples/4); 
%D1pulse(numSamples/4,1);
%D1pulse(numSamples/4,1)
%D1pulse(numSamples/4,1)% *

%D1pulse(numSamples/4); 
%D1pulse(numSamples,1);
%D1pulse(numSamples/4,1)
%D1pulse(numSamples/4,1)% *

for rn=1:numSamples
     D1pulse(1,1);
end

toc