function SimExecuteX (g, params, x0, nmax, nnsize, fname, bplotfig)

ndim = max(size(x0));
t = 0:1:nmax;
yy = g(t,x0);
yy = yy';

if bplotfig == 1
    figure
    plot(yy)
    ttl = compose ("model name: %s, dim: %1d, nmax: %3d, y(n)", fname, ndim, nmax);
    title (ttl)
    
    if ndim ==1
        figure
        plot (yy(1:end-1), yy(2:end), 'b.')
        title ("x_{n+1} (x_n)")
    elseif  ndim == 2
        figure
        plot (yy(:,1), yy(:,2), 'b.')
        title ("y_{n}(x_n)");
    end
end
%global data
%data = yy;
writematrix(yy,fname) ;

end