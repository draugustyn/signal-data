
function C0ModelRucklideA515(numexp)
% Rucklide attractor

% model params
params.k = 2;
params.l = 6.7;
f = @(t,x) [-params.k*x(1)+params.l*x(2)-x(2)*x(3); x(1) ; -x(3)+x(2)*x(2) ];

%experiment params
x0 = [1;0;4.5];
tmax = 150;
nnsize = 1000;

% SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end

end



