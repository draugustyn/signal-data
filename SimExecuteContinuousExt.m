function SimExecuteContinuousExt (f, params, x0, tmax, nnsize, fname, numexp)

global pPlotFig
%pPlotFig = 1

sFolder = strcat('Data-',fname,'/');
mkdir (sFolder)
for i =1:numexp
        sCurrTime = string(datetime ('now','Format','-yyyy-MM-dd-HH-mm-ss-SSS'));
        sFileName = strcat(sFolder,fname,sCurrTime);
        x00 = (2*rand() -1) * abs(x0);
        SimExecuteContinuous(f, params, x00, tmax, nnsize, sFileName, pPlotFig);  
end

end