%       
% Continuous chaotic systems 
%

tic
numSamples= 100 ;

global pPlotFig
pPlotFig = 0 ;

%3D models
% C0ModelLorenzA51(numSamples);
% 
% C0ModelRoslerA52(numSamples);
% 
% C0ModelHalvorsenA513(numSamples) ; 
% 
% %%% not recommended 
% %C0ModelLabyrinthA64(numSamples);
% 
% C0ModelRucklideA515(numSamples);
% 
% %2D models
% C0ModelUedaA45(numSamples);
% 
% 
% %
% % Continuous non-chaotic systems 
% %
% 
% % 2D models - oscilators
% 
% C1LinearOscillator01(numSamples);
% 
% C1LinearOscillator02(numSamples);
% 
% 
% % 3D models - fading 
% 
% C2LinearFading01(numSamples);
% 
% C2LinearFading02(numSamples);

XC01(numSamples);

XC02(numSamples);

XC03(numSamples);

XC04(numSamples);

XC05(numSamples);

XC09a(numSamples);

toc

