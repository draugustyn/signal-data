
function C1LinearOscillator01(numexp)
% Linear oscillator 01

% model params
params.dummy = 0;
f = @(t,x) [  0.5*x(2); - 0.5 *x(1)];

%experiment params
x0 = [ 1;1];
tmax = 100;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);

if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end

end



