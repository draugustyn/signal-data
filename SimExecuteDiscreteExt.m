function SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, fname, numexp)

global pPlotFig
%pPlotFig = 1



sFolder = strcat('Data-',fname,'/');
mkdir (sFolder)
for i =1:numexp
        sCurrTime = string(datetime ('now','Format','-yyyy-MM-dd-HH-mm-ss-SSS'));
        sFileName = strcat(sFolder,fname,sCurrTime);
        x00 = rand() * x0;
        %SimExecuteDiscrete(f, params, x00, tmax, nnsize, sFileName, pPlotFig);  
        SimExecuteDiscrete (g, params, x00, nmax, nnsize, sFileName, pPlotFig)
end

end