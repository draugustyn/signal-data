
function C0ModelHalvorsenA513(numexp)
% Halvorsen attractor

% model params
params.a = 1.27;
f = @(t,x) [-params.a*x(1)-4*x(2)-4*x(3)-x(2)*x(2); -params.a*x(2)-4*x(3)-4*x(1)-x(3)*x(3); -params.a*x(3)-4*x(1)-4*x(2)-x(1)*x(1)];

%experiment params
x0 = [-5;0;0];
tmax = 50;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end

end



