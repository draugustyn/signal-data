figure
% sekewncyjnie lub randomowo 
l = length(si);
sFolder = 'Data-S01';
NFiles = 1000;
mkdir(sFolder)
for i = 1:NFiles
    start =  ceil ((l- 2000) * rand());
    sg2000 = si((start+1):  start+ 2000);
    sg1000 = resample (sg2000,1,2);
    plot (sg1000)
    hold on
    sCurrTime = string(datetime ('now','Format','-yyyy-MM-dd-HH-mm-ss-SSS'));
    fname = strcat('Data-S01','/S01',sCurrTime,'-',num2str(i) );
    writematrix(sg1000,fname) 
end
    