
function D1pulse(numexp, parrandom)
% Pulse signal

% model params
params.A = 0.1;
params.B = 0;
params.C = 2;

if nargin>1
    params.A =  (2 * rand() + 0.25 )* params.A ;
    params.B = 2*pi()/params.A*rand();
    params.C = params.C * (1 + 0.5*rand());
end   

g = @(n,x) [ sign(sin(n*params.A-params.B))*params.C]

%experiment params
x0 = [2];
nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



