
function XC05(numexp)


% model params
params.w1 = 2;
params.w2 = 2*pi;
x0 = [1000];

g = @(t,xx0) [ cos(50*(1+ t*0.01).*t/1000 + xx0(1)) .* sin(t/100 + xx0(1))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



