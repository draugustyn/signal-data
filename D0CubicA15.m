
function D0CubicA15(numexp)
% Cubic map 

% model params
params.A = 3;

g = @(n,x) [ x(1)*params.A*(1 - x(1)*x(1))];

%experiment params
x0 = [0.1];
nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



