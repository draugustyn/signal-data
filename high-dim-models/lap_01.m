load C0ModelLorenzA51-1.txt
xdata = C0ModelLorenzA51_1(:,1)
dim = 3
[~,lag] = phaseSpaceReconstruction(xdata,[],dim)
fs = 100
eRange = 200
lyapunovExponent(xdata, 1,lag,dim,'ExpansionRange',eRange)