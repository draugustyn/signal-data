% Parameters of  Kuramoto-Sivashinsky (KS)
L = 100;                % domain lenght
Nx = 64;                % nubmer of points in grid ...256
dx = L / (Nx - 1);      % inverval in grid
x = linspace(0, L, Nx); % linear space
T = 10;                 % Simulation time
dt = 0.001;             % time step size
Nt = T / dt;            % number of 

% Initial values
u = 0.4 * randn(Nx, 1); % initial disorder
u_next = zeros(Nx, 1);

% Operators of finite differences
e = ones(Nx, 1);
% D2 = spdiags([e -2*e e], -1:1, Nx, Nx) / dx^2;
% D4 = spdiags([e -4*e 6*e -4*e e], -2:2, Nx, Nx) / dx^4;

D2 = spdiags([e -e e], -1:1, Nx, Nx) / (dx^2);  % Less diffusion
D4 = spdiags([e -2*e 3*e -2*e e], -2:2, Nx, Nx) / (dx^4);  % Less diffusion

%  RHS function for KS equations
function du = KS_rhs(u, D2, D4, dx)
    nonlinear = -(u .* ([u(2:end); u(1)] - [u(end); u(1:end-1)]) / (2*dx));
    du = nonlinear + D2 * u - D4 * u;
end

% Initial visualisation
figure;
plot(x, u, 'b', 'LineWidth', 1.5);
title('Initial state');
xlabel('x'); ylabel('u(x, 0)');
axis([0 L -1 1]);
drawnow;
uu = [];
% time loop
for t = 1:Nt
    uu = [uu ; u'];
    % RK4 integral method
    k1 = dt * KS_rhs(u, D2, D4, dx);
    k2 = dt * KS_rhs(u + 0.5*k1, D2, D4, dx);
    k3 = dt * KS_rhs(u + 0.5*k2, D2, D4, dx);
    k4 = dt * KS_rhs(u + k3, D2, D4, dx);
    u_next = u + (k1 + 2*k2 + 2*k3 + k4) / 6;

    % boudary conditions
    u_next(1) = 0;
    u_next(end) = 0;

    % Update of u
    u = u_next;

    % Visualisation after 100 steps
    if mod(t, 100) == 0
        plot(x, u, 'b', 'LineWidth', 1.5);
        title(['Time: ', num2str(t * dt)]);
        xlabel('x'); ylabel('u(x, t)');
        axis([0 L -2 2]);
        drawnow;
    end
end
% final file containing Nx-dimensional data
writematrix(uu,"KS.txt") ;