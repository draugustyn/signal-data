%
% Dicrete chaotic systems 
%


%1D model

D0LogisticA11();    % *

D0CubicA15();       % *


%2D model

D0HenonA21();       % *

D0HenonQadraticA32() ;

%
% Dicrete non-chaotic systems 
%


% 1D model - saw oscillator
D1saw();            % *


% 1D model - pulse oscillator
D1pulse();          % *