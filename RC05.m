
function RC05(numexp)
% quasi-oscillations no 3

% model params
ee = exp(1);

params.w1 = ee;
params.w2 = 1;   
x0 = [2*pi];


g = @(t,xx0) [ sin(params.w1*t/15 + xx0(1)) + cos(params.w2*t/15 + xx0(1))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



