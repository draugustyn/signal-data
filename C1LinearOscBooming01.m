
function C1LinearOscBooming01(numexp)
% Linear osc booming 01

% model params
params.dummy = 0;
f = @(t,x) [x(2);  +0.08*x(2)-0.2*x(1)+0.01]

%experiment params
x0 = [ 0.1; 0.1];
tmax = 100;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



