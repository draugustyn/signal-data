
function RC03(numexp)
% quasi-oscillations no 1

% model params
params.w1 = 2;
params.w2 = 2*pi;   
x0 = [2*pi];

g = @(t,xx0) [ cos(pi*t/50 + xx0(1)) + cos(t/50 + xx0(1))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



