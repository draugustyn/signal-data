
function XC01(numexp)


% model params
params.w1 = 2;
params.w2 = 2*pi;
x0 = [1000;1000];

g = @(t,xx0) [ 2*sin(params.w1./100.*t + xx0(1)) ; 2*sin(params.w2./1000.*t + xx0(2))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



