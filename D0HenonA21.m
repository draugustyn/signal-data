
function D0HenonA21(numexp)
% Henon map 

% model params
params.a = 1.4;
params.b = 0.3;

g = @(n,x) [ 1-params.a*x(1)*x(1)+params.b*x(2), x(1)];

%experiment params
x0 = [0;0.9];
nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



