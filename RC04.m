
function RC04(numexp)
% quasi-oscillations no 2

% model params
sqrt5 = sqrt(5);
gl = (1 + sqrt5)/5;

params.w1 = gl;
params.w2 = 1;   
x0 = [2*pi];


g = @(t,xx0) [ sin(params.w1*t/15 + xx0(1)) + sin(params.w2*t/15 + xx0(1))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



