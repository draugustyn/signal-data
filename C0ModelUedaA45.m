
function C0ModelUedaA45(numexp)
% Ueda oscillator

% model params
params.b = 0.05;
params.A = 7.5;
params.Omega = 1;

f = @(t,x) [x(2); -x(1)*x(1)*x(1)-params.b*x(2)+params.A*sin(params.A*t)];

%experiment params
x0 = [2.5;0];
tmax = 100;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);

if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end

end



