
ss = sy (index2(2): index2(2)+1000)
zeroone = abs(ss) <= 5
dzeroone = diff (zeroone)
adzeroone = dzeroone
adzeroone (adzeroone == -1) = 0
sum(adzeroone)
24

% Rucklide

ruck = load ("C0ModelRucklideA515.txt")
zeroone = abs(ruck(:,1)) <= 0.5
dzeroone = diff (zeroone)
adzeroone = dzeroone
adzeroone (adzeroone == -1) = 0
sum(adzeroone)

49
figure
ss2000 = sy(index2(2): index2(2)+2000)
plot (ss2000)
hold on
plot (ss2000, 'ro')
axis([1 2000 -100 50])
ss1000 = resample (ss2000,1,2)
figure
plot (ss1000)
hold on
plot (ss1000, 'ro')
axis([1 1000 -100 50])

