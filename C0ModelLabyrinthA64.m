
function C0ModelLabyrinthA64(numexp)
% Labyrinth chaos

% model params
params.a = 1.27;
f = @(t,x) [sin(x(2)); sin(x(3)); sin(x(1))];

%experiment params
x0 = [0.1;0;0];
tmax = 1200;
nnsize = 1000 ;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



