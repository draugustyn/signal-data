
function D0HenonQadraticA32(numexp)
% Henon area-preserving quadratic map 

% model params
params.cosalfa = 0.24;
params.sinalfa = sqrt(1 - 0.24^2);

g = @(n,x) [ x(1)*params.cosalfa-(x(2)-x(1)*x(1))*params.sinalfa; x(1)*params.sinalfa+(x(2)-x(1)*x(1))*params.cosalfa];

%experiment params
x0 = [0.6;0.13];
nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



