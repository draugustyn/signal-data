function SimExecuteContinuous (f, param, x0, tmax, nnsize, fname, bplotfig)

ndim = max(size(x0));
opt =  odeset('RelTol',1e-7);
[tspan,x] = ode45(f,[0 tmax],x0, opt); 
 y = interp1 (tspan, x, linspace(0,tmax,nnsize));
if bplotfig == 1
    figure
    plot (tspan, x)
    ttl = compose ("model name: %s, dim: %1d, tmax: %3.2f, x(tspan)", fname, ndim, tmax);
    title (ttl)

    figure
    if ndim==2
        plot ( x(:,1), x(:,2));
        ttl = compose ("model name: %s, dim: %1d, x_2(x_1)", fname, ndim);
        title (ttl)
    elseif ndim >=3
        plot3( x(:,1), x(:,2), x(:,3));
        ttl = compose ("model name: %s, dim: %1d, x_3(x_1, x_2)", fname, ndim);
        title (ttl)
    %   title ("x_3(x_1, x_2)")
    end
   
    figure
    plot (y)
    ttl = compose ("model name: %s, dim: %1d, sample size: %3d", fname, ndim, nnsize);
    title (ttl)
    %save (fname, 'y', '-ascii')
    legend
end
writematrix(y,fname) ;
end