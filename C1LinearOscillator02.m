
function C1LinearOscillator02(numexp)
% Linear oscillator 02

% model params
params.dummy = 0;
f = @(t,x) [  0.5*x(2); - 0.5 *x(1)];

%experiment params
x0 = [ 0.9;-0.9];
tmax = 250;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);
if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



