%       
% Continuous chaotic systems 
%

tic
numSamples= 1000 ;


global pPlotFig
pPlotFig = 0 ;

%3D models
%C0ModelLorenzA51(numSamples);

%C0ModelRoslerA52(numSamples);

%C0ModelHalvorsenA513(numSamples) ; 

%%% not recommended 
%C0ModelLabyrinthA64(numSamples);

%C0ModelRucklideA515(numSamples);

%2D models
%C0ModelUedaA45(numSamples);


%
% Continuous non-chaotic systems 
%

% 2D models - oscilators

%C1LinearOscillator01(numSamples);

%C1LinearOscillator02(numSamples);


% 3D models - fading 

%C2LinearFading01(numSamples);

%C2LinearFading02(numSamples);


% C1LinearOscFading01(numSamples);
% 
% C1LinearOscFading02(numSamples);
% 
% C1LinearOscBooming01(numSamples);
% 
% 
% XC03(numSamples);
% 
% XC04(numSamples);

RC03(numSamples);

RC04(numSamples);

RC05(numSamples);

toc
