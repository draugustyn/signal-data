
function C0ModelLorenzA51(numexp)
% Lorenz attractor 

% model params
params.sigma = 10;
params.beta = 8/3;
params.rho = 28;
f = @(t,x) [-params.sigma*x(1) + params.sigma*x(2); params.rho*x(1) - x(2) - x(1)*x(3); -params.beta*x(3) + x(1)*x(2)];

%experiment params
x0 = [10;20;10];
tmax = 100;
nnsize = 1000;

%SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename);

if nargin == 0
    SimExecuteContinuous(f, params, x0, tmax, nnsize, mfilename, 1);
else
    SimExecuteContinuousExt (f, params, x0, tmax, nnsize, mfilename, numexp)
end


end



