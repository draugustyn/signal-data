
function D1saw(numexp, parrandom)
% Saw signal

% model params
params.A = 8;
params.B = 0;

if nargin>1
    params.A =  ceil((2 * rand() + 0.25 )* params.A) ;
    params.B = floor(params.A/0.2*rand());
end    

g = @(n,x) [ (mod(0.2*(n-params.B),params.A)+1-params.A/2)];

%experiment params
x0 = [params.A/2];
nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end


end



