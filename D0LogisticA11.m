
function D0LogisticA11(numexp, parrandom)
% Logistic map 

% model params
params.A = 4;

if nargin>1
    params.A = (4- 3.57)* rand()+3.57
end    

g = @(n,x) [ x(1)*params.A*(1 - x(1))];

%experiment params
x0 = [0.1];
x0 = [0.9];

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);

if nargin == 0
    SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteDiscreteExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end



end



