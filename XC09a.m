
function XC09a(numexp)
 

% model params
params.w1 = 2;
params.w2 = 2*pi;
x0 = [1000;1000];

g = @(t,xx0) [ 2*sin(cos (50*(t/500)).*(t/500) + xx0(1)) ; 2*sin(cos (50*(t/500)) .*(pi*sqrt(2)*t/500)+ pi/2 + xx0(2))];

%experiment params

nmax = 1000;
nnsize = 1000; % redundant

%SimExecuteDiscrete(g, params, x0, nmax, nnsize, mfilename);
if nargin == 0
    SimExecuteX(g, params, x0, nmax, nnsize, mfilename, 1);
else
    SimExecuteXExt (g, params, x0, nmax, nnsize, mfilename, numexp)
end

end



